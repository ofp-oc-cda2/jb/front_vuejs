FROM node:14.14.0

RUN npm install -g @vue/cli-service

WORKDIR /vue_app

#COPY ./vue_app/ .

#RUN npm install

CMD ["npm", "run", "serve"]