import { createApp } from 'vue'
import App from './App.vue'
import router from './router';
import 'primeicons/primeicons.css';
import PrimeVue from 'primevue/config';
import ('primevue/resources/themes/saga-blue/theme.css');
import ('primevue/resources/primevue.min.css');
import TabMenu from 'primevue/tabmenu';
import Button from 'primevue/button';
import Card from 'primevue/card';
import InputText from 'primevue/inputtext';
import Calendar from 'primevue/calendar';



const app = createApp(App);



app.use(router);

app.use(PrimeVue);

app.component('TabMenu', TabMenu);

app.component('Card', Card);

app.component('Button', Button);

app.component('InputText', InputText);

app.component('Calendar', Calendar);

app.mount('#app')